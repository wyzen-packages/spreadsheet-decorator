<?php

/**
 * Class decorator de Spreadsheet
 * @author deverre
 * @version
 */

declare(strict_types=1);

namespace Wyzen\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\Shared\Date as Shared_Date;
use Wyzen\Php\Helper;

/**
 *
 * composer require phpoffice/phpspreadsheet
 *
 */
class SpreadsheetDecorator extends Spreadsheet
{
    private ?string $message   = null;
    public ?string $uploadDir  = null;
    public ?string $filename   = null;
    public $outputfile         = null;
    public array $defaultTheme = [];
    public array $theme        = [];

    protected $regexTag     = '/#(.+)#/';
    protected $regexRowsTag = '/@(.+)@/';
    private $regexDATE      = '/^(\d{4})-(\d{1,2})-(\d{1,2})/';
    private $regexTIME      = '/(\d{1,2}):(\d{1,2}):(\d{1,2})$/';

    // Info sur la cellule
    public ?Worksheet $currentSheet = null;

    public array $beginEndCell = [];

    // STATUS
    protected bool $statusInsertNewRowBefore = true; // Pour un tableau, insert des lignes automatiquement

    // Cellule begin/end pour les calculs
    protected $cell = [
        'current'   => 'A1',
        'rowindex'  => 1,
        'colindex'  => 1,
        'colletter' => 'A',
    ];

    /**
     * constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder(new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder());
        $this->uploadDir  = \sys_get_temp_dir();
        $this->filename   = \basename(__FILE__);
        $this->outputfile = $this->uploadDir . '/' . $this->filename;
        $this->outputfile = \str_replace('.php', '.xlsx', $this->outputfile);
        $this->addTheme($this->getDefaultTheme());
    }

    /**
     * Charge un fichier Excel depuis un template
     *
     * @param String $file
     *
     * @return Spreadsheet|null
     */
    public function loadFromTemplate(string $file): ?Spreadsheet
    {
        $spreadsheet   = null;
        $inputFileType = 'Xlsx';
        try {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $reader->setIncludeCharts(true);
            $spreadsheet = $reader->load($file);

            $this->attach($spreadsheet);
            $this->removeSheetByIndex(0);
            $this->setActiveSheetIndex(0);
            $this->setProperties($spreadsheet->getProperties());
        } catch (\Throwable $th) {
            $this->message = $th->getMessage();
            return null;
        }

        return $spreadsheet;
    }

    /**
     * Rempli la feuille Excel avec des données depuis des tags
     *
     * @param array $data
     * @param integer|null $pIndex si NULL => Toutes les sheets
     *
     * @return self
     * @throws Exception
     */
    public function fillTemplate(array &$data, ?int $pIndex = null): self
    {
        if (empty($data)) {
            return $this;
        }

        /**
         * Parcours toutes les sheets si pIndex=null, sinon uniquement pIndex
         */
        $sheets = \is_null($pIndex) ? $this->getAllSheets() : [$this->getSheet($pIndex)];

        /**
         * Parcours des sheets
         */
        foreach ($sheets as $sheet) {
            /*
            * Parcours de toutes les cellules
            */
            foreach ($sheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                /*
                * This loops all cells, even if it is not set.
                * By default, only cells that are set will be iterated.
                */
                $cellIterator->setIterateOnlyExistingCells(true);

                //Pour chaque cellules
                /** @var Cell $cell */
                foreach ($cellIterator as $cell) {
                    $this->fillCellByTag($cell, $data);
                }
            }
        }
        return $this;
    }

    /**
     * Indique s'il faut ajouter des lignes automatiquement
     * lors de l'écriture d'un tableau
     *
     * @param boolean $status
     *
     * @return self
     */
    public function setInsertNewRowBefore(bool $status = true): self
    {
        $this->statusInsertNewRowBefore = $status;
        return $this;
    }

    /**
     * Remplace les tag des cellules par leur valeur
     *
     * @param \PhpOffice\PhpSpreadsheet\Cell\Cell $cell Cell
     * @param array $data Array assoc which contains data
     *
     * @return void
     * @throws Exception
     */
    public function fillCellByTag(Cell &$cell, array &$data = []): void
    {
        $sheet      = $cell->getWorksheet();
        $row        = $cell->getRow();
        $coordinate = $cell->getCoordinate();

        $cellValue = $cell->getValue();

        if (!\is_string($cellValue)) {
            return;
        }
        $match = \preg_match($this->regexTag, $cellValue, $matches);

        if (empty($cellValue) || $match === 0) {
            return;
        }

        /** Nom du tag */
        $tag   = $matches[1];
        $value = Helper::getValueFromTag($tag, $data);

        if (\is_array($value)) {
            $nbligne = count($value);
            if ($nbligne > 1 && $this->statusInsertNewRowBefore) {
                $sheet->insertNewRowBefore($row + 1, $nbligne - 1);
            }
            $sheet->fromArray(\array_values($value), null, $coordinate);
            return;
        }

        /**
         * Test si le format est un format DATE ou TIME, sinon on écrit la valeur
         */
        if (\preg_match($this->regexTIME, "$value")) {
            $cell->setValueExplicit('=TIMEVALUE("' . $value . '")', DataType::TYPE_FORMULA);
        } elseif (\preg_match($this->regexDATE, "$value")) {
            $cell->setValueExplicit('=DATEVALUE("' . $value . '")', DataType::TYPE_FORMULA);
        } else {
            $cell->setValue($value);
        }
    }

    /**
     * Attache des sheets au document courrant
     *
     * @param \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet
     * @param bool $append default(true) Ajoute les sheets à la fin
     * @return self
     * @throws Exception
     */
    public function attach(Spreadsheet &$spreadsheet, bool $append = true): self
    {
        $sheets = $spreadsheet->getAllSheets();

        $nbsheet = count($sheets);

        for ($i = 0; $i < $nbsheet; $i++) {
            /** @var Worksheet $sheet */
            $sheet =  ($sheets[$i]);

            $index = null;
            $this->addExternalSheet($sheet, $index);

            $currentSheet = $this->getSheet($i);
            $charts       = $sheet->getChartCollection();
            foreach ($charts as $chart) {
                $chart->setWorksheet($currentSheet);
                $currentSheet->addChart($chart);
            }
        }
        return $this;
    }

    /**
     * write data to cell
     *
     * @param mixed $value string, int, array (if type=callable)
     * @param callable|null $fct
     * @param array $addStyles
     *
     * @return self
     * @throws Exception
     */
    public function writeToCell($value, ?callable $fct = null, array $addStyles = []): self
    {
        $finalValue = $value;
        $styles     = [
            'defaultFont',
            'cell',
        ];

        $styles = array_merge($styles, $addStyles);

        // Test si callback
        if (is_callable($fct)) {
            $finalValue = \is_array($value) ? \call_user_func_array($fct, $value) : \call_user_func_array($fct, [$value]);
        }

        $this->setCurCellValue($finalValue, ['styles' => $styles]);

        return $this;
    }

    /**
     *
     * ████████╗██╗  ██╗███████╗███╗   ███╗███████╗
     * ╚══██╔══╝██║  ██║██╔════╝████╗ ████║██╔════╝
     *    ██║   ███████║█████╗  ██╔████╔██║█████╗
     *    ██║   ██╔══██║██╔══╝  ██║╚██╔╝██║██╔══╝
     *    ██║   ██║  ██║███████╗██║ ╚═╝ ██║███████╗
     *    ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚══════╝
     * @return array
     * $this 'fill' => array(
     *         'type' => Fill::FILL_GRADIENT_LINEAR,
     *         'rotation' => 0,
     *         'startcolor' => array(
     *         'rgb' => '000000'
     *         ),
     *         'endcolor' => array(
     *         'argb' => 'FFFFFFFF'
     *         )
     *         )
     *         'font' => array(
     *         'name' => 'Arial',
     *         'bold' => true,
     *         'italic' => false,
     *         'underline' => Font::UNDERLINE_DOUBLE,
     *         'strike' => false,
     *         'color' => array(
     *         'rgb' => '808080'
     *         )
     *         ),
     *
     *         'alignment' => array(
     *         'horizontal' => Alignment::HORIZONTAL_CENTER,
     *         'vertical' => Alignment::VERTICAL_CENTER,
     *         'rotation' => 0,
     *         'wrapText' => TRUE
     *         ),
     *
     *         'numberformat' => array(
     *         'code' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE
     *         )
     *
     *         'borders' => array(
     *         'bottom' => array(
     *         'style' => Border::BORDER_DASHDOT,
     *         'color' => array(
     *         'rgb' => '808080'
     *         )
     *         ),
     *         'top' => array(
     *         'style' => Border::BORDER_DASHDOT,
     *         'color' => array(
     *         'rgb' => '808080'
     *         )
     *         )
     *         ),
     *         'quotePrefix' => true
     *         )
     */
    public function getDefaultTheme(): array
    {
        /**
         * Base default styles
         */
        $theme =  [
            'defaultFont' => [
                'font' => [
                    'color' => [
                        'rgb' => '000000',
                    ],
                    'size'  => '10',
                ],
            ],

            'h1' => [
                'font'      => [
                    'bold' => true,
                    'size' => '20',
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical'   => Alignment::VERTICAL_CENTER,
                ],
            ],

            'h2' => [
                'font'      => [
                    'bold' => true,
                    'size' => '18',
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical'   => Alignment::VERTICAL_CENTER,
                ],
            ],

            'h3' => [
                'font'      => [
                    'bold' => true,
                    'size' => '16',
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical'   => Alignment::VERTICAL_CENTER,
                ],
            ],

            'h4' => [
                'font'      => [
                    'bold' => true,
                    'size' => '14',
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_LEFT,
                    'vertical'   => Alignment::VERTICAL_CENTER,
                ],
            ],

            'h5' => [
                'font'      => [
                    'bold' => true,
                    'size' => '12',
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_LEFT,
                    'vertical'   => Alignment::VERTICAL_CENTER,
                ],
            ],

            'align-left'   => [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_LEFT,
                ],
            ],
            'align-center' => [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ],
            ],
            'align-right'  => [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                ],
            ],
            'align-middle' => [
                'alignment' => [
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
            ],
            'align-top'    => [
                'alignment' => [
                    'vertical' => Alignment::VERTICAL_TOP,
                ],
            ],
            'align-bottom' => [
                'alignment' => [
                    'vertical' => Alignment::VERTICAL_BOTTOM,
                ],
            ],

            'date' => [
                'alignment'    => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ],
            ],

            'time' => [
                'alignment'    => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ],
            ],

            'euro2deci' => [
                'numberFormat' => [
                    'formatCode' => '# ###0.00_-[$€ ]',
                ],
                'alignment'    => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                ],
            ],

            'euro' => [
                'numberFormat' => [
                    'formatCode' => '# ###0_-[$€ ]',
                ],
                'alignment'    => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                ],
            ],

            'number' => [
                'numberFormat' => [
                    'formatCode' => '#,##0',
                ],
                'alignment'    => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                ],
            ],

            '2deci' => [
                'numberFormat' => [
                    'formatCode' => NumberFormat::FORMAT_NUMBER_00,
                ],
                'alignment'    => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT,
                ],
            ],

            'cell' => [
                'alignment' => [
                    'vertical' => Alignment::VERTICAL_TOP,
                ],
            ],

            'wrap' => [
                'alignment' => [
                    'wrapText' => true,
                ],
            ],
        ];

        /**
         * Custom cell styles
         */
        $theme['cell-title']  = array_replace_recursive($theme['cell'], $theme['align-center'], [
            'font' => [
                'bold' => true,
            ],
        ]);
        $theme['cell-center'] = array_replace_recursive($theme['cell'], $theme['align-center'], $theme['align-top']);
        $theme['cell-left']   = array_replace_recursive($theme['cell'], $theme['align-top']);
        $theme['cell-right']  = array_replace_recursive($theme['cell'], $theme['align-right'], $theme['align-top']);
        $theme['cell-text']   = array_replace_recursive($theme['cell'], $theme['wrap']);
        return $theme;
    }

    /*
   ██████╗██╗   ██╗██████╗ ██████╗ ███████╗███╗   ██╗████████╗     ██████╗███████╗██╗     ██╗
  ██╔════╝██║   ██║██╔══██╗██╔══██╗██╔════╝████╗  ██║╚══██╔══╝    ██╔════╝██╔════╝██║     ██║
  ██║     ██║   ██║██████╔╝██████╔╝█████╗  ██╔██╗ ██║   ██║       ██║     █████╗  ██║     ██║
  ██║     ██║   ██║██╔══██╗██╔══██╗██╔══╝  ██║╚██╗██║   ██║       ██║     ██╔══╝  ██║     ██║
  ╚██████╗╚██████╔╝██║  ██║██║  ██║███████╗██║ ╚████║   ██║       ╚██████╗███████╗███████╗███████╗
   ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝        ╚═════╝╚══════╝╚══════╝╚══════╝
     */

    /**
     * Charge le theme utilisateur
     *
     * @param array $aTheme
     *
     * @return $this
     */
    public function addTheme(array $aTheme = [])
    {
        $this->theme = array_replace_recursive($this->defaultTheme, $aTheme);
        return $this;
    }

    /**
     * Recupere le style de $stylename
     *
     * @param mixed $stylename (string|array)
     *
     * @return array
     */
    public function getStyle($stylename = null): array
    {
        $styles = $this->theme;

        $arrParam = is_array($stylename) ? $stylename : func_get_args();

        $nbStyle = count($arrParam);

        for ($i = 0; $i < $nbStyle; $i++) {
            $value = $arrParam[$i];
            if (array_key_exists($value, $this->theme)) {
                $styles = array_replace_recursive($styles, $this->theme[$value]);
            }
        }

        return $styles;
    }

    /*
  ███╗   ███╗ ██████╗ ██╗   ██╗███████╗     ██████╗███████╗██╗     ██╗
  ████╗ ████║██╔═══██╗██║   ██║██╔════╝    ██╔════╝██╔════╝██║     ██║
  ██╔████╔██║██║   ██║██║   ██║█████╗      ██║     █████╗  ██║     ██║
  ██║╚██╔╝██║██║   ██║╚██╗ ██╔╝██╔══╝      ██║     ██╔══╝  ██║     ██║
  ██║ ╚═╝ ██║╚██████╔╝ ╚████╔╝ ███████╗    ╚██████╗███████╗███████╗███████╗
  ╚═╝     ╚═╝ ╚═════╝   ╚═══╝  ╚══════╝     ╚═════╝╚══════╝╚══════╝╚══════╝
     */

    /**
     * Retourne la cellule courrante
     *
     * @param string $field : current(default), rowindex, colindex, colletter
     *
     * @return string|int
     */
    public function getCurCell($field = 'current')
    {
        switch ($field) {
            case 'current':
            case 'colletter':
                return $this->cell[$field];
            case 'rowindex':
            case 'colindex':
                return \intval($this->cell[$field]);
            default:
                return "A1";
        }
    }

    /*
  ██████╗  █████╗ ███╗   ██╗ ██████╗ ███████╗     ██████╗███████╗██╗     ██╗     ███████╗
  ██╔══██╗██╔══██╗████╗  ██║██╔════╝ ██╔════╝    ██╔════╝██╔════╝██║     ██║     ██╔════╝
  ██████╔╝███████║██╔██╗ ██║██║  ███╗█████╗      ██║     █████╗  ██║     ██║     ███████╗
  ██╔══██╗██╔══██║██║╚██╗██║██║   ██║██╔══╝      ██║     ██╔══╝  ██║     ██║     ╚════██║
  ██║  ██║██║  ██║██║ ╚████║╚██████╔╝███████╗    ╚██████╗███████╗███████╗███████╗███████║
  ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝     ╚═════╝╚══════╝╚══════╝╚══════╝╚══════╝
     */

    /**
     * Deplacement de la cellule courante
     *
     * @param string|array $mixed left, right, up, down, rowstart, colstart, right*4
     *
     * @return SpreadsheetDecorator
     * @throws Exception
     * @throws \Exception
     */
    public function moveTo($mixed)
    {
        $aMove = [];

        $aMove = (\is_string($mixed)) ? [$mixed] : $mixed;

        $coord = Coordinate::coordinateFromString($this->getCurCell());

        $coord['colindex'] = Coordinate::columnIndexFromString($coord[0]);
        $coord['rowindex'] = $coord[1];

        $count = count($aMove);
        for ($i = 0; $i < $count; $i++) {
            // Test si un operateur est attribué. Ex: right*4, remplace 'right','right','right','right'
            $withOp      = false;
            $explodeMove = [];
            if (preg_match(
                /** @lang regex */
                '/^(?<dir>\w+)((?<op>[\*]{1})(?<opnum>\d+))*/i',
                $aMove[$i],
                $explodeMove
                )) {
                $withOp = true;
                if (!array_key_exists('opnum', $explodeMove)) {
                    $explodeMove['opnum'] = 1;
                }
            }

            // Test la direction
            switch ($explodeMove['dir']) {
                case 'left':
                    if ($coord['colindex'] > 1) {
                        $coord['colindex'] -= ($withOp) ? $explodeMove['opnum'] : 1;
                    }
                    break;

                case 'right':
                    $coord['colindex'] += ($withOp) ? $explodeMove['opnum'] : 1;
                    break;

                case 'up':
                    if ($coord['rowindex'] > 1) {
                        $coord['rowindex'] -= ($withOp) ? $explodeMove['opnum'] : 1;
                    }
                    break;

                case 'down':
                case 'bottom':
                    $coord['rowindex'] += ($withOp) ? $explodeMove['opnum'] : 1;
                    break;

                case 'rowstart':
                    $coord['colindex'] = 1;
                    break;

                case 'colstart':
                    $coord['rowindex'] = 1;
                    break;

                default: //move right
                    $coord['colindex'] += ($withOp) ? $explodeMove['opnum'] : 1;
                    break;
            }
        }

        $coord['colletter'] = Coordinate::stringFromColumnIndex($coord['colindex']);

        $this->setCurCell($coord['colletter'] . $coord['rowindex']);

        return $this;
    }

    /**
     * Sauvegarde la cellule courrante
     *
     * @param string $alphanum
     *
     * @return SpreadsheetDecorator
     * @throws Exception
     */
    public function setCurCell($alphanum = 'A1')
    {
        $this->cell['current'] = $alphanum;

        $coord = Coordinate::coordinateFromString($alphanum);

        $this->cell['rowindex']  = $coord[1];
        $this->cell['colindex']  = Coordinate::columnIndexFromString($coord[0]);
        $this->cell['colletter'] = $coord[0];

        return $this;
    }

    /**
     * Memorise la cellule de depart
     *
     * @param string         $alphanum
     * @param string         $name
     * @param Worksheet|null $sheet
     *
     * @return SpreadsheetDecorator
     * @throws Exception
     */
    public function setBeginCell($alphanum = 'A1', $name = 'default', Worksheet $sheet = null)
    {
        $this->beginEndCell[$name]['begin'] = $alphanum;
        if (!isset($this->beginEndCell[$name]['sheet']) && is_null($sheet)) {
            $sheet = $this->getCurrentSheet();
        }
        $this->beginEndCell[$name]['sheet'] = $sheet;

        return $this;
    }



    /**
     * Memorise la cellule de fin
     *
     * @param string         $alphanum
     * @param string         $name
     * @param Worksheet|null $sheet
     *
     * @return SpreadsheetDecorator
     * @throws Exception
     */
    public function setEndCell($alphanum = 'A1', $name = 'default', Worksheet $sheet = null)
    {
        $this->beginEndCell[$name]['end'] = $alphanum;
        if (!isset($this->beginEndCell[$name]['sheet']) && is_null($sheet)) {
            $sheet = $this->getCurrentSheet();
        }
        $this->beginEndCell[$name]['sheet'] = $sheet;

        return $this;
    }

    /**
     * Retourne le debut, la fin, et la plage
     *
     * @param string $name
     *
     * @return object
     */
    public function getRange($name = 'default')
    {
        $ret = [
            'begin' => $this->beginEndCell[$name]['begin'],
            'end'   => $this->beginEndCell[$name]['end'],
            'range' => $this->beginEndCell[$name]['begin'] . ':' . $this->beginEndCell[$name]['end'],
        ];

        return (object) $ret;
    }

    /**
     * set Header
     *
     * @param array $options
     *
     * @return self
     */
    public function setHeader(array $options = []): self
    {
        $this->getProperties()
            ->setCreator(Helper::getValueFromTag('creator', $options))
            ->setTitle(Helper::getValueFromTag('title', $options))
            ->setSubject(Helper::getValueFromTag('subject', $options))
            ->setDescription(Helper::getValueFromTag('description', $options))
            ->setCategory(Helper::getValueFromTag('category', $options));

        return $this;
    }

    /*
   ██████╗ ██╗   ██╗████████╗██████╗ ██╗   ██╗████████╗
  ██╔═══██╗██║   ██║╚══██╔══╝██╔══██╗██║   ██║╚══██╔══╝
  ██║   ██║██║   ██║   ██║   ██████╔╝██║   ██║   ██║
  ██║   ██║██║   ██║   ██║   ██╔═══╝ ██║   ██║   ██║
  ╚██████╔╝╚██████╔╝   ██║   ██║     ╚██████╔╝   ██║
   ╚═════╝  ╚═════╝    ╚═╝   ╚═╝      ╚═════╝    ╚═╝
     */

    /**
     * Ecriture dans un fichier
     *
     * @param string $filename
     *
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function writeToFile($filename = 'document.xlsx')
    {
        // Save Excel
        $objWriter = IOFactory::createWriter($this, 'Xlsx');

        // $objWriter->setIncludeCharts(true);

        try {
            $objWriter->save($filename, \PhpOffice\PhpSpreadsheet\Writer\IWriter::SAVE_WITH_CHARTS);
        } catch (\Exception $ex) {
            $this->message = $ex->getMessage();
            return false;
        }

        return true;
    }

    /**
     * Téléchargement du document
     *
     * @param string $filename
     * @param string $writerType
     *
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function saveAs($filename = 'document', $writerType = 'Xlsx')
    {
        $ext    = '.' . strtolower($writerType);
        $output = $filename . '.' . date('Y-m-d') . $ext;
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $output . '"');

        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = IOFactory::createWriter($this, $writerType);
        $objWriter->setIncludeCharts(true);

        $objWriter->save('php://output');
        exit();
    }

    /**
     * Convertion au format DATE EXCEL
     *
     * @param string $strdate YYYY-MM-DD
     *
     * @return string|false|float
     */
    public function date2Excel($strdate)
    {
        if ($strdate == "") {
            return '';
        }

        return Shared_Date::stringToExcel($strdate);
    }

    /**
     * Ecrit dans la cellule courante
     * Usage: $this->setCurCellValue($sheet, utf8('Pas de données'), ['styles'=>['h4','align-center','align-middle']]);
     *
     * @param mixed $content
     * @param array  $aparam
     *
     * @return SpreadsheetDecorator
     * @throws Exception
     */
    public function setCurCellValue($content = null, $aparam = [])
    {
        $datatype = DataType::TYPE_STRING;
        if (\is_null($content)) {
            $datatype = DataType::TYPE_NULL;
        } elseif (\is_bool($content)) {
            $datatype = DataType::TYPE_BOOL;
        } elseif (\is_numeric($content)) {
            $datatype = DataType::TYPE_NUMERIC;
        }

        $this->getCurrentSheet()->setCellValueExplicit($this->getCurCell(), $content, $datatype);

        $styles = ['defaultFont'];

        if (array_key_exists('styles', $aparam)) {
            $styles = array_merge($styles, $aparam['styles']);

            $this->getCurrentSheet()->getStyle($this->getCurCell())->applyFromArray($this->getStyle($styles));
        } else {
            $this->getCurrentSheet()->getStyle($this->getCurCell())->applyFromArray($this->getStyle($styles));
        }

        return $this;
    }

    /**
     * Configure la largeur d'une colonne
     *
     * @param int $width
     *
     * @return SpreadsheetDecorator
     * @throws Exception
     */
    public function setCellWidth($width = 10): self
    {
        $this->getCurrentSheet()->getColumnDimension($this->cell['colletter'])->setWidth($width);
        return $this;
    }

    /**
     * Set Row Height
     *
     * @param int $height
     * @return SpreadsheetDecorator
     */
    public function setCellHeight($height = -1): self
    {
        $this->getCurrentSheet()->getRowDimension(intval($this->getCurCell('rowindex')))->setRowHeight($height);
        return $this;
    }

    /**
     * Création du sheet
     * {@inheritDoc}
     */
    public function createSheet($sheetIndex = null): Worksheet
    {
        $this->setCurrentSheet(parent::createSheet($sheetIndex));

        return $this->getCurrentSheet();
    }

    /**
     * @return Worksheet|null
     */
    public function getCurrentSheet(): ?Worksheet
    {
        if (is_null($this->currentSheet)) {
            $this->setCurrentSheet(parent::getActiveSheet());
        }

        return $this->currentSheet;
    }

    /**
     * Set le sheet courant
     *
     * @param Worksheet $currentSheet
     *
     * @return $this
     */
    public function setCurrentSheet(Worksheet $currentSheet)
    {
        $this->currentSheet = $currentSheet;

        return $this;
    }

    /**
     * return message
     *
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Copy a range cells to another destination
     *
     * @param Worksheet $sheet
     * @param string $srcRange
     * @param string $dstCell
     * @param Worksheet|null $destSheet . null is for the same sheet
     *
     * @return void
     * @throws Exception
     */
    public static function copyRange(Worksheet $sheet, string $srcRange, string $dstCell, Worksheet $destSheet = null): void
    {

        if (!isset($destSheet)) {
            $destSheet = $sheet;
        }

        if (!preg_match('/^([A-Z]+)(\d+):([A-Z]+)(\d+)$/', $srcRange, $srcRangeMatch)) {
            // Invalid src range
            return;
        }

        if (!preg_match('/^([A-Z]+)(\d+)$/', $dstCell, $destCellMatch)) {
            // Invalid dest cell
            return;
        }

        $srcColumnStart = $srcRangeMatch[1];
        $srcRowStart    = intval($srcRangeMatch[2]);
        $srcColumnEnd   = $srcRangeMatch[3];
        $srcRowEnd      = intval($srcRangeMatch[4]);

        $destColumnStart = $destCellMatch[1];
        $destRowStart    = intval($destCellMatch[2]);

        $srcColumnStartIndex  = Coordinate::columnIndexFromString($srcColumnStart);
        $srcColumnEndIndex    = Coordinate::columnIndexFromString($srcColumnEnd);
        $destColumnStartIndex = Coordinate::columnIndexFromString($destColumnStart);

        $rowCount = 0;
        for ($row = $srcRowStart; $row <= $srcRowEnd; $row++) {
            $colCount = 0;
            for ($col = $srcColumnStartIndex; $col <= $srcColumnEndIndex; $col++) {
                $cell       = $sheet->getCell([$col, $row]);
                $style_cell = $sheet->getStyle([$col, $row]);

                /**
                 * Récup les différents styles: font, border, align ...
                 */
                $all_styles = $style_cell->exportArray();

                $dstCell_string = Coordinate::stringFromColumnIndex($destColumnStartIndex + $colCount) . (string)($destRowStart + $rowCount);
                $destSheet->setCellValue($dstCell_string, $cell->getValue());

                /**
                 * Application des styles
                 */
                $destStyle = $destSheet->getStyle($dstCell_string);
                $destStyle->applyFromArray($all_styles);

                // Set width of column, but only once per column
                if ($rowCount === 0) {
                    $w = $sheet->getColumnDimensionByColumn($col)->getWidth();
                    $destSheet->getColumnDimensionByColumn($destColumnStartIndex + $colCount)->setAutoSize(false);
                    $destSheet->getColumnDimensionByColumn($destColumnStartIndex + $colCount)->setWidth($w);
                }

                $colCount++;
            }

            $h = $sheet->getRowDimension(intval($row))->getRowHeight();
            $destSheet->getRowDimension($destRowStart + $rowCount)->setRowHeight($h);

            $rowCount++;
        }

        /**
         * Reproduction des cellules fusionnées
         */
        foreach ($sheet->getMergeCells() as $mergeCell) {
            $mc = explode(":", $mergeCell);
            $mergeColSrcStart = Coordinate::columnIndexFromString(preg_replace("/[0-9]*/", "", $mc[0]));
            $mergeColSrcEnd   = Coordinate::columnIndexFromString(preg_replace("/[0-9]*/", "", $mc[1]));
            $mergeRowSrcStart = ((int)preg_replace("/[A-Z]*/", "", $mc[0]));
            $mergeRowSrcEnd   = ((int)preg_replace("/[A-Z]*/", "", $mc[1]));

            $relativeColStart = $mergeColSrcStart - $srcColumnStartIndex;
            $relativeColEnd   = $mergeColSrcEnd - $srcColumnStartIndex;
            $relativeRowStart = $mergeRowSrcStart - $srcRowStart;
            $relativeRowEnd   = $mergeRowSrcEnd - $srcRowStart;

            if (0 <= $mergeRowSrcStart && $mergeRowSrcStart >= $srcRowStart && $mergeRowSrcEnd <= $srcRowEnd) {
                $targetColStart = Coordinate::stringFromColumnIndex($destColumnStartIndex + $relativeColStart);
                $targetColEnd   = Coordinate::stringFromColumnIndex($destColumnStartIndex + $relativeColEnd);
                $targetRowStart = $destRowStart + $relativeRowStart;
                $targetRowEnd   = $destRowStart + $relativeRowEnd;

                $merge = (string)$targetColStart . (string)($targetRowStart) . ":" . (string)$targetColEnd . (string)($targetRowEnd);
                //Merge target cells
                $destSheet->mergeCells($merge);
            }
        }
    }

    public static function copyStyleXFCollection(Spreadsheet $sourceSheet, Spreadsheet $destSheet)
    {
        $collection = $sourceSheet->getCellXfCollection();

        foreach ($collection as $item) {
            $destSheet->addCellXf($item);
        }
    }
}
