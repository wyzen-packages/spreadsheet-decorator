# Wyzen\Spreadsheet PHPOffice/PhpSpreadsheet Decorator

[Spreadsheet documentation](https://phpspreadsheet.readthedocs.io)

## Import

```bash
composer require wyzen-packages/spreadsheet-decorator
```

## usage

```php
use Wyzen\Spreadsheet\SpreadsheetDecorator;
```

## Sample, load from template

```php
$excel = new SpreadsheetDecorator();
$excel->loadFromTemplate($template_file);

$excel->fillTemplate($data, 0);
$excel->fillTemplate($dataSheetTwo, 1);
$excel->setActiveSheetIndex(0);

$excel->setCurCell('A2')
    ->setCellWidth(40)
    ->setCellHeight(25)
    ->writeToCell('Manual value', null, ['h1', 'center'])
    ->getCurrentSheet()->mergeCells('A2:F2');

$excel->setCurCell('D3')
    ->writeToCell([true, 'second parameter'], __CLASS__ . '::myCallbackFunction', ['align-right']);
$excel->setCurCell('E3')
    ->writeToCell(true, __CLASS__ . '::YesNo', ['align-center']);

$excel->setCurCell('A2');
$excel->writeToFile($file_out_write_to_cells);


$sheet1 = $excel->getSheet(0);
$sheet2 = $excel->getSheet(1);
$excel->copyRange($sheet1, 'C9:F13', 'C23');
$excel->copyRange($sheet1, 'C9:F13', 'C22', $sheet2);
```

## Liste des méthodes

- all from Spreadsheet
- loadFromTemplate
- fillTemplate
- setInsertNewRowBefore
- fillCellByTag
- attach
- writeToCell
- getDefaultTheme
- addTheme
- getStyle
- getColId
- getRowId
- getCurCell
- setCurCell
- setCurCellValue
- moveTo
- setBeginCell
- setEndCell
- getRange
- setHeader
- writeToFile
- saveAs
- date2Excel
- setCellWidth
- setCellHeight
- createSheet
- getCurrentSheet
- setCurrentSheet
- copyRange

## Issues

- some format characters
- some bug with picture (with border, shadow)
