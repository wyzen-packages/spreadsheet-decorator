<?php

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PHPUnit\Framework\TestCase;
use Wyzen\Spreadsheet\SpreadsheetDecorator;

class SpreadsheetDecoratorTest extends TestCase
{
    private static $file_out  = __DIR__ . '/file_out_document.xlsx';
    private static $file_read = __DIR__ . '/data_to_read.xlsx';
    private static $file_out_filled_template           = __DIR__ . '/file_out_filled_template.xlsx';
    private static $file_out_filled_template_onesheet  = __DIR__ . '/file_out_filled_template_onesheet.xlsx';
    private static $file_out_filled_template_allsheets = __DIR__ . '/file_out_filled_template_allsheets.xlsx';
    private static $file_out_write_to_cells            = __DIR__ . '/file_out_write_to_cells.xlsx';
    private static $file_out_write_image__to_cells            = __DIR__ . '/file_out_write_image_to_cells.xlsx';
    private static $file_copyRange_template            = __DIR__ . '/copyRange.xlsx';
    private static $file_out_copyRange_template        = __DIR__ . '/file_out_copyRange_template.xlsx';
    private static $image = __DIR__ . '/microsoft-excel-icon.png';

    private static $template_file = __DIR__ . '/template.xltx';

    private static $data         = [];
    private static $dataSheetTwo = [];


    public static function setUpBeforeClass(): void
    {
        self::removeFiles();

        self::$data = [
            'date' => \date('Y-m-d'),
            'time' => \date('H:i:s'),
            'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porta ornare pharetra.
            Aenean consequat odio ut leo iaculis consequat. Fusce scelerisque dapibus mi in faucibus.
            Pellentesque quis aliquet libero. Nullam a auctor tortor, ac egestas orci.
            Quisque interdum tempus velit non convallis. Nam viverra purus in molestie sollicitudin.
            Integer pellentesque lacus porta eleifend feugiat. Pellentesque habitant morbi tristique
            senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consectetur
            adipiscing elit. Praesent leo tortor, iaculis nec eros et, malesuada pellentesque elit.',
            'items' => [
                'list1' => [
                    'ref1' => [
                        'item' => 'item name 1',
                        'value' => 123.78,
                        'qty' => 2,
                        'total' => 123.78 * 2,
                    ],
                    'ref2' => [
                        'item' => 'item name 2',
                        'value' => 23.99,
                        'qty' => 3,
                        'total' => 23.99 * 3,
                    ],
                ],
                'list2' => [
                    [
                        'list 2 item name 1',
                        66.8,
                        6,
                        66.8 * 6,
                    ],
                    // [
                    //      'liste2 item name 2',
                    //      23.99,
                    //      3,
                    //      23.99 * 3,
                    // ],
                ],
            ]
        ];

        self::$dataSheetTwo = [
            'date' => \date('Y-m-d'),
            'time' => \date('H:i:s'),
            'text' => 'Text sheet two',
            'items' => [
                'list2' => [
                    [
                        'list 2 item name 1',
                        66.8,
                        6,
                        66.8 * 6,
                    ],
                    [
                        'liste2 item name 2',
                        23.99,
                        3,
                        23.99 * 3,
                    ],
                ],
            ]
        ];
    }
    public static function tearDownAfterClass(): void
    {
        // self::removeFiles();
    }

    public static function removeFiles()
    {
        if (\file_exists(self::$file_out)) {
            \unlink(self::$file_out);
        }
    }

    public function testConstruct()
    {
        $this->assertInstanceOf(SpreadsheetDecorator::class, new SpreadsheetDecorator());
    }

    public function testCreate()
    {
        $excel = new SpreadsheetDecorator();
        $this->assertInstanceOf(SpreadsheetDecorator::class, $excel);
    }

    public function testWriteToFile()
    {
        $excel = new SpreadsheetDecorator();
        $excel->writeToFile(self::$file_out);
        $this->assertFileExists(self::$file_out);
    }

    public function testLoadTemplate()
    {
        $excel = new SpreadsheetDecorator();

        $ss = $excel->loadFromTemplate(self::$template_file);

        $sheet = $excel->getSheetByName('Sheet1');

        $this->assertNotNull($sheet);

        $result = $sheet->getCell('B1');

        $excel->writeToFile(self::$file_out);
        $this->assertFileExists(self::$file_out);
    }

    public function testReadXlxs()
    {
        $excel = new SpreadsheetDecorator();
        $ss    = $excel->loadFromTemplate(self::$file_read);

        $rows = $excel->getActiveSheet()->toArray();

        $this->assertIsArray($rows);
    }

    public function testFillTemplate()
    {
        $excel = new SpreadsheetDecorator();
        $excel->loadFromTemplate(self::$template_file);

        $excel->fillTemplate(self::$data);

        $excel->setActiveSheetIndex(0);
        $excel->setCurCell('A2');
        $excel->writeToFile(self::$file_out_filled_template);
        $this->assertFileExists(self::$file_out_filled_template);
    }

    public function testFillTemplateOneSheet()
    {
        $excel = new SpreadsheetDecorator();
        $excel->loadFromTemplate(self::$template_file);

        $excel->fillTemplate(self::$dataSheetTwo, 1);

        $excel->setActiveSheetIndex(1);
        $excel->setCurCell('A2');
        $excel->writeToFile(self::$file_out_filled_template_onesheet);
        $this->assertFileExists(self::$file_out_filled_template_onesheet);
    }

    public function testFillTemplateSheets()
    {
        $excel = new SpreadsheetDecorator();
        $excel->loadFromTemplate(self::$template_file);

        $excel->fillTemplate(self::$data, 0);
        $excel->fillTemplate(self::$dataSheetTwo, 1);

        $excel->setActiveSheetIndex(0);
        $excel->setCurCell('A2');
        $excel->writeToFile(self::$file_out_filled_template_allsheets);
        $this->assertFileExists(self::$file_out_filled_template_allsheets);
    }

    public function testCopyRange()
    {
        $excel = new SpreadsheetDecorator();
        $excel->loadFromTemplate(self::$file_copyRange_template);
        $excel->setActiveSheetIndex(0);
        $sheet1 = $excel->getSheet(0);
        $sheet2 = $excel->getSheet(1);
        $excel->copyRange($sheet1, 'A6:F20', 'H23');
        $excel->copyRange($sheet1, 'C9:F13', 'H22', $sheet2);
        $excel->writeToFile(self::$file_out_copyRange_template);
        $this->assertFileExists(self::$file_out_copyRange_template);
    }

    public function testWriteToCell()
    {
        $excel = new SpreadsheetDecorator();
        $excel->loadFromTemplate(self::$template_file);

        $excel->fillTemplate(self::$data, 0);
        $excel->fillTemplate(self::$dataSheetTwo, 1);
        $excel->setActiveSheetIndex(0);

        $excel->setCurCell('A2')
            // ->setCellWidth(40)
            ->setCellHeight(25)
            ->writeToCell('Manual value', null, ['h1', 'center'])
            ->getCurrentSheet()->mergeCells('A2:F2');

        $excel->setCurCell('A3')
            ->moveTo(['bottom', 'right*2',])
            ->writeToCell('Manual value with moveTo C4', null, ['align-right']);

        $excel->moveTo('right*4, left');

        $excel->setCurCell('D3')
            ->writeToCell(true, __CLASS__ . '::YesNo', ['align-center']);

        $excel->setCurCell('E3')
            ->writeToCell([false, 'second parameter'], __CLASS__ . '::myCallbackFunction');

        $excel->setCurCell('A2');
        $excel->writeToFile(self::$file_out_write_to_cells);
        $this->assertFileExists(self::$file_out_write_to_cells);
    }

    public function testWriteImageToCell()
    {
        $excel = new SpreadsheetDecorator();
        $excel->loadFromTemplate(self::$template_file);

        $excel->setActiveSheetIndex(0);

        $excel->setCurCell('G9');

        $drawing = new Drawing();
        $drawing->setName('Image');
        $drawing->setDescription('Image Description');
        $drawing->setPath(self::$image);
        $drawing->setCoordinates('G9'); // Set the cell where the top-left corner of the image will be placed
        $drawing->setHeight(100); // Set the height of the image
        $offsetY = $drawing->getOffsetY();
        $drawing->setOffsetY($offsetY + 20);
        $drawing->setWorksheet($excel->getActiveSheet());

        $excel->writeToFile(self::$file_out_write_image__to_cells);
        $this->assertFileExists(self::$file_out_write_image__to_cells);
    }


    // /**
    //  * custom value from parameters
    //  *
    //  * @param mixed $value1
    //  * @param mixed $value2
    //  *
    //  * @return mixed
    //  */
    /**
     * callback custom function
     *
     * @param bool $value1
     * @param string|bool|int $value2
     *
     * @return string
     */
    public static function myCallbackFunction(bool $value1, $value2): string
    {
        $calcValue1 = self::YesNo($value1);
        return __FUNCTION__ . " results: $calcValue1, $value2";
    }

    /**
     * callback custom function
     *
     * @param bool $status
     *
     * @return string
     */
    public static function YesNo(bool $status): string
    {
        $calcValue1 = ($status === true) ? 'TRUE' : 'FALSE';
        return $calcValue1;
    }
}
